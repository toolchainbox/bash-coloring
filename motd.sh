#!/bin/bash

cat << "EOF" > /etc/motd
,____
|   / _____   _____
|  / / __/ | / / _ |
| / _\ \ | |/ / __ |
|/ /___/ |___/_/ |_|

@SVAGmbH | #SVAGmbH | www.sva.de

EOF
