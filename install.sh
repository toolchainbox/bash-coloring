#!/bin/bash

mkdir ~/bin/

cat << "EOF" > ~/bin/color
# Ansi color code variables
red="\e[0;91m"
blue="\e[0;94m"
expand_bg="\e[K"
blue_bg="\e[0;104m${expand_bg}"
red_bg="\e[0;101m${expand_bg}"
green_bg="\e[0;102m${expand_bg}"
green="\e[0;92m"
white="\e[0;97m"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"
EOF

cat << "EOF" > ~/bin/msginfo
#!/bin/bash
source ~/bin/color
echo -e "${blue}${bold}INFO:${reset} $1"
EOF

cat << "EOF" > ~/bin/msgwarn
#!/bin/bash
source ~/bin/color
echo -e "${red}${bold}WARNING:${reset} $1"
EOF

cat << "EOF" > ~/bin/msgok
#!/bin/bash
source ~/bin/color
echo -e "${green}${bold}OK:${reset} $1"
EOF

chmod +x ~/bin/msginfo ~/bin/msgwarn ~/bin/msgok



msgok "Color Mode installed!"

